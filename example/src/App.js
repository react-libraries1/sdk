import React, {Fragment} from 'react'

import { Navbar, Thimble6, Thimble } from '@verybadcodes-org/sdk'

const App = () => {
  return (
    <>
      <Navbar title="Titre de la navbar"/>
      <Thimble min={1} max={10}/>
    </>
  ) 
}

export default App
