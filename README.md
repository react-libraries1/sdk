# @verybadcodes-org/sdk

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/@verybadcodes-org/sdk.svg)](https://www.npmjs.com/package/@verybadcodes-org/sdk) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @verybadcodes-org/sdk
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@verybadcodes-org/sdk'
import '@verybadcodes-org/sdk/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [verybadcodes](https://github.com/verybadcodes)
