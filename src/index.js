import { Navbar as MaterialNavbar} from './material-ui/navbar/navbar';
import { Thimble as MaterialThimble, Thimble6 as MaterialThimble6} from './material-ui/thimble/Thimble';


export const Navbar = MaterialNavbar
export const Thimble = MaterialThimble
export const Thimble6 = MaterialThimble6


