import React, { useEffect, useState } from 'react'
import thimble1 from 'thimble1.png'
import thimble2 from 'thimble2.png'
import thimble3 from 'thimble3.png'
import thimble4 from 'thimble4.png'
import thimble5 from 'thimble5.png'
import thimble6 from 'thimble6.png'

function getRandom(min, max) {
    return Math.floor((Math.random() * ((max)*10 - min) + min) / 10)+1;
}



export const Thimble6 = (props) => {

    const [value, setValue] = useState(1)
    const [thimble, setImg] = useState()

    const min = 1
    const max = 6

    let i = 0

    const diceRolle = () => {
        setTimeout(()=>{
            setValue(getRandom(min,max))
            if(i<10){
                diceRolle()
                i++
            }else{
                i = 0
            }
        }, 50)
    }

    const format = (value) => {
        switch(value){
            case 1:
                return <img src={thimble1}/>
            case 2:
                return <img src={thimble2}/>
            case 3:
                return <img src={thimble3}/>
            case 4:
                return <img src={thimble4}/>
            case 5:
                return <img src={thimble5}/>
            case 6:
                return <img src={thimble6}/>
        }
    }


    return (
        <div style={{cursor: 'pointer', display: 'inline-block'}} onClick={diceRolle}>
            {format(value)}
        </div>
    )
}

export const Thimble = (props) => {

    const [value, setValue] = useState(props.min)

    const min = props.min
    const max = props.max
    
    if(min === undefined || max === undefined){
        throw 'min and max value of thimble must not be null'
    }

    let i = 0

    const diceRolle = () => {
        setTimeout(()=>{
            setValue(getRandom(min,max))
            if(i<10){
                diceRolle()
                i++
            }else{
                i = 0
            }
        }, 50)
    }
    

    return (
        <div style={{cursor:'pointer', display:'inline-block', width:'80px', height:'80px', fontSize:'30px', border: 'solid 1px black', textAlign:'center'}} onClick={diceRolle}>
            <p style={{margin:0, width:'100%', height:'100%', padding: '17px', textAlign: 'center'}}>{value}</p>
        </div>
    )
}